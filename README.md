# Virtualspirit Test

## Prequisite

- ruby

## How To

- Clone this repository

```
git clone https://gitlab.com/mamxalf/virtualspirit-test.git
```

- Enter inside root directory program (depends on your folder/path)

```
~/virtualspirit-test
```

- Execute the Program

```
ruby content_counter.rb /your/path/want-to-scan
```
