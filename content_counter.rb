require 'digest'

class ContentCounter
	attr_reader :files_by_content

	def initialize(path)
		@path = path
		@files_by_content = {}
	end

	def analyze
		find_files(@path).each do |file_path|
			next unless FileTest.file?(file_path)
			content = Digest::SHA256.file(file_path).hexdigest
			files_by_content[content] ||= []
			files_by_content[content] << file_path
		end
	end

	def find_max_content
		content, _ = files_by_content.max_by { |_, path| path.count }
		content
	end

	def max_count
		files_by_content.values.map(&:count).max
	end

	private

	def find_files(path)
		Dir.glob("#{path}/**/*")
	end
end

# executor
if ARGV.empty?
	puts 'Please provide a path as a command-line argument.'
else
	result = ContentCounter.new(ARGV[0])
	# analyze path
	result.analyze
	puts "#{result.find_max_content} #{result.max_count}"
end