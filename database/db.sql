CREATE TABLE Users (
    ID INT PRIMARY KEY,
    Name VARCHAR(50),
    Mark CHAR(1)
);

INSERT INTO Users (ID, Name, Mark)
VALUES (1, 'John', 'A'),
       (2, 'Marissa', 'B'),
       (3, 'Bob', 'C'),
       (4, 'Britany', 'C');

WITH RecursiveMarks AS (
    SELECT Mark
    FROM Users

    UNION

    SELECT CONCAT(u1.Mark, '+', u2.Mark) AS Mark
    FROM Users u1
    JOIN Users u2 ON u1.ID < u2.ID
)
SELECT
    Mark,
    COUNT(*) AS Count
FROM RecursiveMarks
GROUP BY Mark
ORDER BY Mark;
