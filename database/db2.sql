CREATE TABLE books (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Date DATE NOT NULL,
    user_id INTEGER NOT NULL
);

CREATE TABLE users (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL
);

INSERT INTO books (ID, Name, Date, user_id) VALUES
(1, 'Bachelor of Information Systems', '2019-01-01', 1),
(2, 'Bachelor of Design', '2019-02-02', 2),
(3, 'Bachelor of Commerce', '2019-03-03', 3),
(4, 'Associate Degree in Health Science', '2019-04-04', 3),
(5, 'Master of Architectural Technology', '2019-05-05', 2),
(6, 'Bachelor of Psychology', '2019-06-06', 2),
(7, 'Associate Degree in Information Systems', '2019-07-07', 1);

INSERT INTO users (ID, Name) VALUES
(1, 'John'),
(2, 'Bob'),
(3, 'Britany');

SELECT 
    u.Name AS User, 
    b.Name AS Book, 
    b.Date AS Date 
FROM 
    users u 
JOIN 
    books b 
ON 
    u.ID = b.user_id 
WHERE 
    (u.ID, b.Date) IN 
    (
        SELECT 
            user_id, 
            MAX(Date) 
        FROM 
            books 
        GROUP BY 
            user_id
    ) 
ORDER BY 
    u.ID;